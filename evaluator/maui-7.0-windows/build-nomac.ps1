$ErrorActionPreference = "Stop"

# Remove MacCatalyst builds where enabled
Write-Output "Disabling MacCatalyst builds."
Get-ChildItem -Path .\ -Filter *.csproj -Recurse -File -Name | ForEach-Object {
  $projectFilePath = "$_"
  Get-Content "$projectFilePath" | % {$_ -replace "(;net7\.0-maccatalyst|net7\.0-maccatalyst;)",""} | Out-File "$projectFilePath.tmp"
  Move-Item -Force "$projectFilePath.tmp" "$projectFilePath"
}

# Build workflow
& C:\build.ps1