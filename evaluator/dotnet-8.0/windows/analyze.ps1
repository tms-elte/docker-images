param (
  [parameter(Mandatory=$false)]
  [String]$arch
)
  
$ErrorActionPreference = "Stop"

$projects = Get-ChildItem -Path .\ -Filter *.csproj -Recurse -File -Name
if ($projects.Count -eq 0) {
  Write-Output "No C# projects found."
  throw "No C# projects found."
}

$diagnostics = @("--supported-diagnostics")
if (Test-Path -Path C:\test\test_files\diagnostics.txt) {
  $diagnostics += Get-Content -Path C:\test\test_files\diagnostics.txt | Where-Object { $_ -notmatch "^#" }
} else {
  $diagnostics += curl.exe --fail --silent --show-error https://gitlab.com/tms-elte/backend-core/-/snippets/2518152/raw/main/diagnostics.txt | Where-Object { $_ -notmatch "^#" }
}

C:\build.ps1 > $null

Write-Output "Running Roslynator"
roslynator analyze $projects `
  --output roslynator.xml `
  --severity-level hidden `
  --ignore-analyzer-references `
  --analyzer-assemblies $env:ANALYZERS_DIR `
  --report-suppressed-diagnostics `
  $diagnostics
$roslynatorExitCode = $LASTEXITCODE

if ($arch -eq "") {
  if (Test-Path -Path "roslynator.xml") {
    Write-Output "Static analysis found issues."
    throw "Static analysis found issues."
  }
  if ($roslynatorExitCode -ne 0) {
    Write-Output "Roslynator execution failed."
    throw "Roslynator execution failed."
  }
} else {  
  [string[]]$solutions = Get-ChildItem -Path .\ -Filter *.sln -Recurse -File -Name
  if ($solutions.Count -eq 0) {
    Write-Output "No C# solutions found."
    throw "No C# solutions found."
  }

  Write-Output "Performing architectural analysis"
  Write-Output $solutions[0]
  ArchitecturalAnalyzer analyze $solutions[0] `
    --architecture $arch `
    --output architecture.xml

  if (Test-Path -Path "roslynator.xml") {
    Rename-Item roslynator.xml roslynator-native.xml
  }

  Write-Output "Merging Roslynator and ArchitecturalAnalyzer results"
  XmlMerger roslynator-native.xml architecture.xml roslynator.xml
  $xmlMergerExitCode = $LASTEXITCODE
  if (Test-Path -Path "roslynator.xml") {
    Write-Output "Static analysis found issues."
    throw "Static analysis found issues."
  }
  if ($xmlMergerExitCode -ne 0) {
    Write-Output "Report output merging failed."
    throw "Report output merging failed."
  }
}
