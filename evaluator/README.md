# TMS Evaluator images

Docker images for the Evaluator component in TMS.

## Available images

### `evaluator:cpp-ubuntu-24.04`
An Ubuntu 24.04 OS with essential build tools (C/C++) and [Ericsson CodeChecker](https://codechecker.readthedocs.io/) added.

Similarly, an `evaluator:cpp-ubuntu-22.04` image with Ubuntu 22.04 as a base image and 
an `evaluator:cpp-ubuntu-20.04` image with Ubuntu 20.04 as a base image exist.

### `evaluator:qt5-ubuntu-20.04`
Extends the `evaluator:cpp-ubuntu-20.04` image with the Qt5 SDK.

Contains a build script to compile Qt projects. Usage:
 - copy the Qt projects into the container in a preferred folder;
 - execute the `/build.sh` script in that folder.

### `evaluator:dotnet-8.0-debian`
Debian 11 Bullseye based image with the .NET 6 SDK (.NET CLI, .NET runtime, ASP.NET Core).

Contains a build script to compile Visual Studio solutions (only .NET Core supported). Usage:
 - copy the Visual Studio solutions into the container in a preferred folder;
 - execute the `/build.sh` script in that folder to build all solutions found;
 - execute the `/execute.sh` script in that folder to run the first found executable project.

Contains `Roslynator.Dotnet.CLI` and the following Roslyn-based analyzers:
 - `Microsoft.CodeAnalysis.NetAnalyzers`
 - `SonarAnalyzer.CSharp`
 - `Roslynator.Analyzers`

Similarly, an `evaluator:dotnet-6.0-debian` image exists for .NET 6.

### `evaluator:dotnet-4.8-windows`
Windows Server, version 2019 OS with the following tools included:
 - .NET Framework 4.8 SDK
 - .NET 7.0 SDK
 - Visual Studio Build Tools
 - Visual Studio Test Agent
 - NuGet CLI

Contains a build script to compile Visual Studio solutions (.NET Framework and .NET Core). Usage:
 - copy the Visual Studio solutions into the container in a preferred folder;
 - execute the `C:\build.ps1` script in that folder.

### `evaluator:dotnet-8.0-windows`
Windows Server, version 2019 OS with the .NET SDK 8.0 included.

Contains a build script to compile Visual Studio solutions (only .NET Core supported). Usage:
 - copy the Visual Studio solutions into the container in a preferred folder;
 - execute the `C:\build.ps1` script in that folder to build all solutions found;
 - execute the `C:\execute.ps1` script in that folder to run the first found executable project.

Contains `Roslynator.Dotnet.CLI` and the following Roslyn-based analyzers:
 - `Microsoft.CodeAnalysis.NetAnalyzers`
 - `SonarAnalyzer.CSharp`
 - `Roslynator.Analyzers`

Similarly, an `evaluator:dotnet-6.0-windows` image exists for .NET 6.

### `evaluator:dotnet-8.0`
Multi-architecture image for `evaluator:dotnet-8.0-debian` and `evaluator:dotnet-8.0-windows`.

Similarly, an `evaluator:dotnet-6.0` image exists for .NET 6.

### `evaluator:maui-6.0-windows`
Extends the `evaluator:dotnet-6.0-windows` image with MAUI support and related tooling:
 - MAUI workload for .NET (for Windows, Android, iOS, macOS, tvOS)
 - Android SDK (API 31 & API 33)
 - Java SDK (OpenJDK 8 & OpenJDK 11)

Contains a `C:\build.ps1` build script to compile Visual Studio solutions.

Similarly, an `evaluator:maui-7.0-windows` image exists for .NET 7.
