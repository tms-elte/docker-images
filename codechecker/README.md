# TMS Ericsson CodeChecker images

Docker images containing a minimal [Ericsson CodeChecker](https://codechecker.readthedocs.io/) installation without the compiler and analyzer toolkit.
These images are intended for running additional tools provided by CodeChecker (e.g., `report-converter`).
If you wish to run static code analysis with CodeChecker, choose a suitable [evaluator](evaluator/) image.

## Supported tags
* `6` (multiarch image tag)
* `6-debian` [Dockerfile](codechecker/6/debian)
* `6-windowsservercore-1809` [Dockerfile](codechecker/6/windowsservercore-1809)

